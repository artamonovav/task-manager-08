package ru.t1.artamonov.tm;

import ru.t1.artamonov.tm.api.ICommandRepository;
import ru.t1.artamonov.tm.model.Command;
import ru.t1.artamonov.tm.repository.CommandRepository;
import ru.t1.artamonov.tm.util.FormatUtil;

import java.util.Scanner;

import static ru.t1.artamonov.tm.constant.ArgumentConst.*;
import static ru.t1.artamonov.tm.constant.TerminalConst.*;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name:    Anatoly Artamonov");
        System.out.println("E-mail:  aartamonov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) System.out.print(command);
   }

    private static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument not supported...");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command not supported...");
    }

    private static void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case CMD_HELP:
                showHelp();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_INFO:
                showInfo();
                break;
            case CMD_EXIT:
                System.exit(0);
                break;
            case CMD_ARGUMENTS:
                showArguments();
                break;
            case CMD_COMMANDS:
                showCommands();
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void processCommands() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("\nENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ARG_HELP:
                showHelp();
                break;
            case ARG_VERSION:
                showVersion();
                break;
            case ARG_ABOUT:
                showAbout();
                break;
            case ARG_INFO:
                showInfo();
                break;
            case ARG_ARGUMENTS:
                showArguments();
                break;
            case ARG_COMMANDS:
                showCommands();
                break;
            default:
                showErrorArgument();
                break;
        }
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length < 1) {
            return false;
        }
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
