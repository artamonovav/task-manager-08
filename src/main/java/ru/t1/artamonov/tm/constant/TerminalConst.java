package ru.t1.artamonov.tm.constant;

public final class TerminalConst {

    public final static String CMD_HELP = "help";

    public final static String CMD_VERSION = "version";

    public final static String CMD_ABOUT = "about";

    public final static String CMD_EXIT = "exit";

    public final static String CMD_INFO = "info";

    public final static String CMD_ARGUMENTS = "arguments";

    public final static String CMD_COMMANDS = "commands";

}